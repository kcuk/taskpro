import { Component, OnInit } from '@angular/core';
import { ArObjService } from '../ar-obj.service';
@Component({
  selector: 'app-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.css']
})
export class OrderComponent implements OnInit {

  constructor(objService:ArObjService) { 
    console.log("FETCHING DATA FROM SERVICE IN THE ORDER COMPONENT",objService.ar);
  }

  ngOnInit() {
  }

}
