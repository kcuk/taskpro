import { Component, OnInit } from '@angular/core';
import { ArObjService } from '../ar-obj.service';
@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {

  arObj=[];
  valQty:boolean = false;
  count:number = 0;
disableMinus:boolean = true;

  constructor(private objService:ArObjService) {
    
    console.log("fetching data of service in the list component",objService.ar);
    this.arObj=objService.ar;
   }

  ngOnInit() {
  }

  plus(event){
    console.log("plus event fired: " ,event);
    this.valQty =true;
    // this.objService.ar.quantity;
    this.count++;
    console.log(this.count);

    this.objService.ar
    this.disableMinus = false;
  }

  minus(event){
    // if(this.count > 0){

    // }
    console.log("minus event fired", event);
    if(this.count>0){
      this.count--;
      console.log("count in the minus(event) method",this.count);
      if(this.count == 0){
        this.disableMinus = true;
      console.log("disable the minus button");        
      }
    }
  }
}
