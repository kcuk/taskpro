import { TestBed } from '@angular/core/testing';

import { ArObjService } from './ar-obj.service';

describe('ArObjService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ArObjService = TestBed.get(ArObjService);
    expect(service).toBeTruthy();
  });
});
